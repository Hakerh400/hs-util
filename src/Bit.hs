module Bit where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative
import Control.Monad

import Util

data Bit = Bit0 | Bit1
  deriving (Eq, Ord, Enum, Bounded)

show_bit :: Bit -> String
show_bit b = case b of
  Bit0 -> "0"
  Bit1 -> "1"

show_bits :: [Bit] -> String
show_bits = (>>= show_bit)

show_bits_s :: [Bit] -> ShowS
show_bits_s bs s = show_bits bs ++ s

instance Show Bit where
  show = show_bit
  showList = show_bits_s

prop_to_bit :: Prop -> Bit
prop_to_bit p = if p then Bit1 else Bit0

bit_to_prop :: Bit -> Prop
bit_to_prop b = b == Bit1

bit_add :: Bit -> Bit -> Bit
bit_add a b = prop_to_bit # a /= b

bit_sub :: Bit -> Bit -> Bit
bit_sub = bit_add

bit_and :: Bit -> Bit -> Bit
bit_and Bit1 Bit1 = Bit1
bit_and _ _ = Bit0

bit_or :: Bit -> Bit -> Bit
bit_or Bit0 Bit0 = Bit0
bit_or _ _ = Bit1

bit_iff :: Bit -> Bit -> Bit
bit_iff Bit1 Bit1 = Bit1
bit_iff Bit0 Bit0 = Bit1
bit_iff _ _ = Bit0

bit_xor :: Bit -> Bit -> Bit
bit_xor Bit0 Bit1 = Bit1
bit_xor Bit1 Bit0 = Bit1
bit_xor _ _ = Bit0

bit_mul :: Bit -> Bit -> Bit
bit_mul = bit_and

inv_bit :: Bit -> Bit
inv_bit b = case b of
  0 -> 1
  _ -> 0

bit_abs :: Bit -> Bit
bit_abs = id

bit_sgn :: Bit -> Bit
bit_sgn = id

integer_to_bit :: Integer -> Bit
integer_to_bit n = prop_to_bit # odd n

bit_to_integer :: Bit -> Integer
bit_to_integer b = case b of
  Bit0 -> 0
  Bit1 -> 1

instance Num Bit where
  (+) = bit_add
  (-) = bit_sub
  (*) = bit_mul
  abs = bit_abs
  signum = bit_sgn
  fromInteger = integer_to_bit

instance Real Bit where
  toRational = error ""

instance Integral Bit where
  quotRem = error ""
  toInteger = bit_to_integer
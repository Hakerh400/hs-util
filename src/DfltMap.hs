module DfltMap where

import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Util

data DfltMap k v = DfltMap
  { _dflt_map_z :: v
  , _dflt_map_mp :: Map k v
  } deriving (Eq, Ord, Show)

map_to_dflt_map_safe :: (Ord k, Eq v) => v -> Map k v -> DfltMap k v
map_to_dflt_map_safe z mp = DfltMap
  { _dflt_map_z = z
  , _dflt_map_mp = mp
  }

dflt_map_to_map :: (Ord k, Eq v) => DfltMap k v -> Map k v
dflt_map_to_map = _dflt_map_mp

map_fn_to_dflt_map_fn :: (Ord k, Eq v) => (Map k v -> Map k v) -> DfltMap k v -> DfltMap k v
map_fn_to_dflt_map_fn f mp = mp {_dflt_map_mp = f # _dflt_map_mp mp}

dflt_map_empty :: (Ord k, Eq v) => v -> DfltMap k v
dflt_map_empty z = map_to_dflt_map_safe z Map.empty

dflt_map_lookup :: (Ord k, Eq v) => k -> DfltMap k v -> v
dflt_map_lookup k mp = Map.findWithDefault
  (_dflt_map_z mp) k # _dflt_map_mp mp

dflt_map_insert :: (Ord k, Eq v) => k -> v -> DfltMap k v -> DfltMap k v
dflt_map_insert k v mp = flip map_fn_to_dflt_map_fn mp #
  if v == _dflt_map_z mp then Map.delete k else Map.insert k v

map_to_dflt_map :: (Ord k, Eq v) => v -> Map k v -> DfltMap k v
map_to_dflt_map z = Map.foldrWithKey dflt_map_insert # dflt_map_empty z
module Trimap where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import Data.Foldable
import Data.Functor.Identity
import Control.Applicative
import Control.Monad
import Control.Monad.State

import Util
import Serializer

data Trimap a b c = Trimap
  { _trimap1 :: Map a (Map b c)
  , _trimap2 :: Map b (Map a c)
  , _trimap3 :: Map c (Set (a, b))
  } deriving (Eq, Ord, Show)

trimap_empty :: Trimap a b c
trimap_empty = Trimap
  { _trimap1 = Map.empty
  , _trimap2 = Map.empty
  , _trimap3 = Map.empty
  }

trimap_get :: (Ord a, Ord b) => a -> b -> Trimap a b c -> Maybe c
trimap_get a b mp = map_get a (_trimap1 mp) >>= map_get b

trimap_get_dflt :: (Ord a, Ord b, Inhabited c) => a -> b -> Trimap a b c -> c
trimap_get_dflt a b mp = maybe_to_dflt # trimap_get a b mp

trimap_has :: (Ord a, Ord b) => a -> b -> Trimap a b c -> Prop
trimap_has a b mp = isJust # trimap_get a b mp

trimap_insert :: (Ord a, Ord b, Ord c) => a -> b -> c -> Trimap a b c -> Trimap a b c
trimap_insert a b c mp = mp
  { _trimap1 = map_map_insert a b c # _trimap1 mp
  , _trimap2 = map_map_insert b a c # _trimap2 mp
  , _trimap3 = map_set_insert c (a, b) # _trimap3 mp
  }

trimap_insert_new :: HCS => (Ord a, Ord b, Ord c) => a -> b -> c -> Trimap a b c -> Trimap a b c
trimap_insert_new a b c mp = mp
  { _trimap1 = map_map_insert_new a b c # _trimap1 mp
  , _trimap2 = map_map_insert_new b a c # _trimap2 mp
  , _trimap3 = map_set_insert_new c (a, b) # _trimap3 mp
  }

trimap_remove :: (Ord a, Ord b, Ord c) => a -> b -> c -> Trimap a b c -> Trimap a b c
trimap_remove a b c mp = mp
  { _trimap1 = map_map_remove a b # _trimap1 mp
  , _trimap2 = map_map_remove b a # _trimap2 mp
  , _trimap3 = map_set_remove c (a, b) # _trimap3 mp
  }

trimap_from_list :: (Ord a, Ord b, Ord c) => [(a, b, c)] -> Trimap a b c
trimap_from_list = foldr <~ trimap_empty # \(a, b, c) ->
  trimap_insert a b c

trimap_to_list :: (Ord a, Ord b, Ord c) => Trimap a b c -> [(a, b, c)]
trimap_to_list mp = do
  (a, mp) <- Map.toList # _trimap1 mp
  (b, c) <- Map.toList mp
  return (a, b, c)

type TrimapOrdT a b c =
  ( Serializable a, Serializable b, Serializable c
  , Ord a, Ord b, Ord c
  )

instance TrimapOrdT a b c => Serializable (Trimap a b c) where
  ser = ser . trimap_to_list
  dser = fmap trimap_from_list dser
module Trie where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import Data.Foldable
import Data.Functor.Identity
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Cont

import Util
import DFS
import Serializer

data Trie k v = Trie
  { _trie_val :: Maybe v
  , _trie_map :: Map k (Trie k v)
  } deriving (Eq, Ord, Show)

trie_empty :: (Ord k) => Trie k v
trie_empty = Trie Nothing Map.empty

instance (Ord k) => Inhabited (Trie k v) where
  dflt = trie_empty

ser_trie :: (Ord k, Serializable k, Serializable v) => Trie k v -> Ser ()
ser_trie mm = do
  ser # _trie_val mm
  ser # _trie_map mm

dser_trie :: (Ord k, Serializable k, Serializable v) => Dser (Trie k v)
dser_trie = do
  val <- dser
  mm <- dser
  return # Trie
    { _trie_val = val
    , _trie_map = mm
    }

instance (Ord k, Serializable k, Serializable v) => Serializable (Trie k v) where
  ser = ser_trie
  dser = dser_trie

trie_insert_fn :: (Ord k) => [k] -> v -> Maybe (Trie k v) -> Maybe (Trie k v)
trie_insert_fn ks v mm = Just # trie_insert ks v # from_maybe mm

trie_insert :: (Ord k) => [k] -> v -> Trie k v -> Trie k v
trie_insert ks v mm = case ks of
  [] -> mm {_trie_val = Just v}
  (k:ks) -> let
    m = _trie_map mm
    m' = Map.alter (trie_insert_fn ks v) k m
    in mm {_trie_map = m'}

trie_singleton :: (Ord k) => [k] -> v -> Trie k v
trie_singleton ks v = trie_insert ks v trie_empty

list_to_trie :: (Ord k) => [([k], v)] -> Trie k v
list_to_trie = foldr (uncurry trie_insert) trie_empty

map_to_trie :: (Ord k) => Map [k] v -> Trie k v
map_to_trie = Map.foldrWithKey trie_insert trie_empty

trie_keys :: (Ord k) => Trie k v -> [[k]]
trie_keys = Map.keys . trie_to_map

trie_elems :: (Ord k) => Trie k v -> [v]
trie_elems = Map.elems . trie_to_map

trie_to_list_aux1 :: (Ord k) => [k] -> Trie k v -> [([k], v)]
trie_to_list_aux1 ks mm = do
  v <- toList # _trie_val mm
  return # (ks, v)

trie_to_list_aux2 :: (Ord k) => [k] -> Trie k v -> [([k], v)]
trie_to_list_aux2 ks mm = do
  (k, mm') <- Map.toList # _trie_map mm
  trie_to_list' (ks ++ [k]) mm'

trie_to_list' :: (Ord k) => [k] -> Trie k v -> [([k], v)]
trie_to_list' ks mm = trie_to_list_aux1 ks mm ++ trie_to_list_aux2 ks mm

trie_to_list :: (Ord k) => Trie k v -> [([k], v)]
trie_to_list = trie_to_list' []

trie_to_map :: (Ord k) => Trie k v -> Map [k] v
trie_to_map mm = Map.fromList # trie_to_list mm

trie_union :: (Ord k) => Trie k v -> Trie k v -> Trie k v
trie_union mm1 mm2 = map_to_trie #
  Map.union (trie_to_map mm1) (trie_to_map mm2)

trie_unions :: (Ord k) => [Trie k v] -> Trie k v
trie_unions mms = map_to_trie #
  Map.unions # map trie_to_map mms

trie_dif :: (Ord k) => Trie k v -> Trie k v -> Trie k v
trie_dif mm1 mm2 = map_to_trie #
  Map.difference (trie_to_map mm1) (trie_to_map mm2)

trie_lookup :: (Ord k) => [k] -> Trie k v -> Maybe v
trie_lookup ks mm = case ks of
  [] -> _trie_val mm
  (k:ks) -> do
    mm <- Map.lookup k # _trie_map mm
    trie_lookup ks mm

trie_member :: (Ord k) => [k] -> Trie k v -> Prop
trie_member ks mm = isJust # trie_lookup ks mm

trie_insert_list :: (Ord k) => [k] -> v -> Trie k [v] -> Trie k [v]
trie_insert_list k v mm = let
  vs = v : fromMaybe [] (trie_lookup k mm)
  in trie_insert k vs mm

trie_filter_prefix :: (Ord k) => [k] -> Trie k v -> Trie k v
trie_filter_prefix ks mm = case ks of
  [] -> mm
  (k:ks) -> let
    mp = _trie_map mm
    mm' = case map_get k mp of
      Nothing -> trie_empty
      Just mm -> trie_filter_prefix ks mm
    in mm {_trie_map = Map.singleton k mm'}

trie_get :: (Ord k) => k -> Trie k v -> Maybe (Trie k v)
trie_get k trie = map_get k # _trie_map trie

trie_next :: (Ord k) => Trie k v -> Maybe (k, Trie k v)
trie_next trie = do
  [k] <- pure # Map.keys # _trie_map trie
  trie <- trie_get k trie
  return (k, trie)

trie_null :: (Ord k) => Trie k v -> Prop
trie_null trie = isNothing (_trie_val trie) && Map.null (_trie_map trie)

trie_nnull :: (Ord k) => Trie k v -> Prop
trie_nnull = not . trie_null

-----

type TrieList k v = Trie k [v]

trie_list_empty :: (Ord k) => TrieList k v
trie_list_empty = trie_empty

trie_list_singleton :: (Ord k) => [k] -> v -> TrieList k v
trie_list_singleton ks v = trie_singleton ks [v]

trie_list_insert :: (Ord k) => [k] -> v -> TrieList k v -> TrieList k v
trie_list_insert ks v mm = let
  xs = case trie_lookup ks mm of
    Nothing -> [v]
    Just xs -> v : xs
  in trie_insert ks xs mm

trie_list_union :: (Ord k) => TrieList k v -> TrieList k v -> TrieList k v
trie_list_union mm1 mm2 = (\f -> foldr f mm1 # trie_to_list mm2) #
  \(ks, xs) mm -> case trie_lookup ks mm of
    Nothing -> trie_insert ks xs mm
    Just ys -> trie_insert ks (xs ++ ys) mm

trie_list_unions :: (Ord k) => [TrieList k v] -> TrieList k v
trie_list_unions = foldr trie_list_union trie_list_empty

-----

type TrieSet k v = Trie k (Set v)

trie_set_empty :: (Ord k, Ord v) => TrieSet k v
trie_set_empty = trie_empty

trie_set_singleton :: (Ord k, Ord v) => [k] -> v -> TrieSet k v
trie_set_singleton ks v = trie_singleton ks # Set.singleton v

trie_set_insert :: (Ord k, Ord v) => [k] -> v -> TrieSet k v -> TrieSet k v
trie_set_insert ks v mm = let
  set = case trie_lookup ks mm of
    Nothing -> Set.singleton v
    Just set -> Set.insert v set
  in trie_insert ks set mm

trie_set_union :: (Ord k, Ord v) => TrieSet k v -> TrieSet k v -> TrieSet k v
trie_set_union mm1 mm2 = (\f -> foldr f mm1 # trie_to_list mm2) #
  \(ks, set) mm -> case trie_lookup ks mm of
    Nothing -> trie_insert ks set mm
    Just set1 -> trie_insert ks (Set.union set set1) mm

trie_set_unions :: (Ord k, Ord v) => [TrieSet k v] -> TrieSet k v
trie_set_unions = foldr trie_set_union trie_set_empty

-----

newtype TrieM k v r m a = TrieM
  { _trie_m :: DFS r (StateT (Trie k v) m) a
  } deriving (Functor, Applicative, Monad)

instance (Ord k) => MonadTrans (TrieM k v r) where
  lift m = TrieM # lift # lift m

instance (Try m, Ord k) => MonadFail (TrieM k v r m) where
  fail s = TrieM # fail s

-- instance (Try m, Ord k) => Try (TrieM k v r m) where
--   try (TrieM m) = TrieM # try m

instance (Try m, MonadIO m, Ord k) => MonadIO (TrieM k v r m) where
  liftIO m = TrieM # liftIO m

trie_m_run :: (Try m, Ord k) => TrieM k v r m r -> Trie k v -> m (Maybe r)
trie_m_run (TrieM m) trie = evalStateT (dfs_run m) trie

trie_m_choose :: (Try m, Ord k) => [a] -> TrieM k v r m a
trie_m_choose xs = TrieM # dfs_choose xs

trie_m_choose_m :: (Try m, Ord k) => [TrieM k v r m a] -> TrieM k v r m a
trie_m_choose_m ms = TrieM # dfs_choose_m # map _trie_m ms

trie_m_next :: (Try m, Ord k) => TrieM k v r m k
trie_m_next = TrieM # do
  xs <- lift # gets # Map.toList . _trie_map
  (k, trie) <- dfs_choose xs
  lift # put trie
  return k

trie_m_exact :: (Try m, Ord k) => k -> TrieM k v r m ()
trie_m_exact k = TrieM # lift # do
  Just trie <- gets # map_get k . _trie_map
  put trie

trie_m_val' :: (Try m, Ord k) => TrieM k v r m (Maybe v)
trie_m_val' = TrieM # lift # gets _trie_val

trie_m_val :: (Try m, Ord k) => TrieM k v r m v
trie_m_val = do
  Just v <- trie_m_val'
  return v

trie_m_eof :: (Try m, Ord k) => TrieM k v r m Prop
trie_m_eof = fmap isJust trie_m_val'

trie_list_m_val :: (Try m, Ord k) => TrieM k [v] r m v
trie_list_m_val = trie_m_val >>= trie_m_choose

trie_set_m_val :: (Try m, Ord k, Ord v) => TrieM k (Set v) r m v
trie_set_m_val = trie_m_val >>= trie_m_choose . Set.toList

trie_m_try :: (Try m, Ord k) => TrieM k v a m a -> TrieM k v r m (Maybe a)
trie_m_try (TrieM m) = TrieM # dfs_try m
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE IncoherentInstances #-}

module JSON.JSON where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.List
import Data.Char
import Control.Monad.State
import Control.Monad.Trans.Except
import GHC.Types

import Util

data JSON
  = Nil
  | Prop Prop
  | Int N
  | Real Double
  | Str String
  | List [JSON]
  | Obj (Map String JSON)
  deriving (Eq, Ord)

type IsChar :: Type -> Bool
type family IsChar a where
  IsChar Char = True
  IsChar a = False

type IsString :: Type -> Bool
type family IsString a where
  IsString String = True
  IsString a = False

uc_escape_char_code' :: N -> String
uc_escape_char_code' n = pad_start 4 '0' # nat_to_hex n

uc_escape_char_code :: N -> String
uc_escape_char_code n = "\\u" ++ uc_escape_char_code' n

uc_escape_char :: Char -> String
uc_escape_char c = uc_escape_char_code # fi # ord c

json_escape_char :: Char -> String
json_escape_char c =
  if elem c "\"\\" then '\\' : [c]
  else if c < ' ' then
    if c == '\r' then "\\r"
    else if c == '\n' then "\\n"
    else uc_escape_char c else [c]

json_escape_str :: String -> String
json_escape_str s = s >>= json_escape_char

json_show_str :: String -> String
json_show_str s = quote # json_escape_str s

show_json' :: JSON -> String
show_json' jn = case jn of
  Nil -> "null"
  Prop p -> ite p "true" "false"
  Int n -> show n
  Real x -> show x
  Str s -> json_show_str s
  List xs -> put_in_brackets # intercalate "," # map show xs
  Obj mp -> put_in_braces # intercalate "," #
    flip map (Map.toList mp) # \(k, v) ->
      concat [json_show_str k, ":", show v]

instance Show JSON where
  show = show_json'

class ToJSON a where
  to_json :: a -> JSON

show_json :: (ToJSON a) => a -> String
show_json = show . to_json

instance ToJSON JSON where
  to_json = id

instance ToJSON Prop where
  to_json = Prop

instance ToJSON Int where
  to_json = Int . fi

instance ToJSON Integer where
  to_json = Int . fi

instance ToJSON Double where
  to_json = Real

instance (ToJSON a, ToJSON b) => ToJSON (a, b) where
  to_json (a, b) = List [to_json a, to_json b]

instance (ToJSON a) => ToJSON (Maybe a) where
  to_json x = case x of
    Just x -> to_json x
    Nothing -> Nil

instance ToJSON String where
  to_json s = Str s

list_to_json :: (ToJSON a) => [a] -> JSON
list_to_json = List . map to_json

instance (ToJSON a, IsChar a ~ False) => ToJSON [a] where
  to_json xs = list_to_json xs

instance (Ord k, ToJSON k) => ToJSON (Set k) where
  to_json = list_to_json . Set.toList

instance (Ord k, ToJSON k, ToJSON v, IsString k ~ False) =>
  ToJSON (Map k v) where
  to_json = list_to_json . Map.toList

instance (ToJSON v) => ToJSON (Map String v) where
  to_json = Obj . Map.map to_json
module Bimap where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import Data.Foldable
import Data.Functor.Identity
import Control.Applicative
import Control.Monad
import Control.Monad.State

import Util
import Serializer

data Bimap a b = Bimap
  { _bimapl :: Map a b
  , _bimapr :: Map b a
  } deriving (Eq, Ord, Show)

data Bimap_1n a b = Bimap_1n
  { _bimapl_1n :: Map a (Set b)
  , _bimapr_1n :: Map b a
  } deriving (Eq, Ord, Show)

data Bimap_nn a b = Bimap_nn
  { _bimapl_nn :: Map a (Set b)
  , _bimapr_nn :: Map b (Set a)
  } deriving (Eq, Ord, Show)

type Bimap_11 = Bimap

class BimapC mp where
  bimap_empty :: (Ord a, Ord b) => mp a b
  bimap_insert :: (Ord a, Ord b) => a -> b -> mp a b -> mp a b

instance BimapC Bimap where
  bimap_empty = Bimap
    { _bimapl = Map.empty
    , _bimapr = Map.empty
    }
  bimap_insert a b mp = Bimap
    { _bimapl = Map.insert a b # _bimapl mp
    , _bimapr = Map.insert b a # _bimapr mp
    }

instance BimapC Bimap_1n where
  bimap_empty = Bimap_1n
    { _bimapl_1n = Map.empty
    , _bimapr_1n = Map.empty
    }
  bimap_insert a b mp = Bimap_1n
    { _bimapl_1n = map_alter' a (_bimapl_1n mp) #
      maybe (Set.singleton b) (Set.insert b)
    , _bimapr_1n = Map.insert b a # _bimapr_1n mp
    }

instance BimapC Bimap_nn where
  bimap_empty = Bimap_nn
    { _bimapl_nn = Map.empty
    , _bimapr_nn = Map.empty
    }
  bimap_insert a b mp = Bimap_nn
    { _bimapl_nn = map_alter' a (_bimapl_nn mp) #
      maybe (Set.singleton b) (Set.insert b)
    , _bimapr_nn = map_alter' b (_bimapr_nn mp) #
      maybe (Set.singleton a) (Set.insert a)
    }

bimap_remove :: (Ord a, Ord b) => a -> Bimap a b -> Bimap a b
bimap_remove a mp = case bimap_get a mp of
  Nothing -> mp
  Just b -> mp
    { _bimapl = Map.delete a # _bimapl mp
    , _bimapr = Map.delete b # _bimapr mp
    }

bimap_1n_remove :: (Ord a, Ord b) => a -> Bimap_1n a b -> Bimap_1n a b
bimap_1n_remove a mp = case bimap_1n_get a mp of
  Nothing -> mp
  Just set -> mp
    { _bimapl_1n = Map.delete a # _bimapl_1n mp
    , _bimapr_1n = set_fold_r set (_bimapr_1n mp) Map.delete
    }

bimap_nn_remove :: (Ord a, Ord b) => a -> Bimap_nn a b -> Bimap_nn a b
bimap_nn_remove a mp = case bimap_nn_get a mp of
  Nothing -> mp
  Just set -> mp
    { _bimapl_nn = Map.delete a # _bimapl_nn mp
    , _bimapr_nn = set_fold_r set (_bimapr_nn mp) # \b mp ->
      map_set_remove b a mp
    }

bimap_size :: (Ord a, Integral n) => Bimap a b -> n
bimap_size = map_size . _bimapl

bimap_1n_size :: (Ord a, Integral n) => Bimap_1n a b -> n
bimap_1n_size = map_size . _bimapl_1n

bimap_1n_size' :: (Ord b, Integral n) => Bimap_1n a b -> n
bimap_1n_size' = map_size . _bimapr_1n

bimap_nn_size :: (Ord a, Integral n) => Bimap_nn a b -> n
bimap_nn_size = map_size . _bimapl_nn

bimap_nn_size' :: (Ord b, Integral n) => Bimap_nn a b -> n
bimap_nn_size' = map_size . _bimapr_nn

bimap_has :: (Ord a) => a -> Bimap a b -> Prop
bimap_has a mp = Map.member a # _bimapl mp

bimap_has' :: (Ord b) => b -> Bimap a b -> Prop
bimap_has' b mp = Map.member b # _bimapr mp

bimap_1n_has :: (Ord a) => a -> Bimap_1n a b -> Prop
bimap_1n_has a mp = Map.member a # _bimapl_1n mp

bimap_1n_has' :: (Ord b) => b -> Bimap_1n a b -> Prop
bimap_1n_has' b mp = Map.member b # _bimapr_1n mp

bimap_nn_has :: (Ord a) => a -> Bimap_nn a b -> Prop
bimap_nn_has a mp = Map.member a # _bimapl_nn mp

bimap_nn_has' :: (Ord b) => b -> Bimap_nn a b -> Prop
bimap_nn_has' b mp = Map.member b # _bimapr_nn mp

bimap_get :: (Ord a) => a -> Bimap a b -> Maybe b
bimap_get a mp = map_get a # _bimapl mp

bimap_get' :: (Ord b) => b -> Bimap a b -> Maybe a
bimap_get' b mp = map_get b # _bimapr mp

bimap_1n_get :: (Ord a) => a -> Bimap_1n a b -> Maybe (Set b)
bimap_1n_get a mp = map_get a # _bimapl_1n mp

bimap_1n_get' :: (Ord b) => b -> Bimap_1n a b -> Maybe a
bimap_1n_get' b mp = map_get b # _bimapr_1n mp

bimap_1n_get_dflt :: (Ord a) => a -> Bimap_1n a b -> Set b
bimap_1n_get_dflt a mp = maybe_to_dflt # bimap_1n_get a mp

bimap_nn_get :: (Ord a) => a -> Bimap_nn a b -> Maybe (Set b)
bimap_nn_get a mp = map_get a # _bimapl_nn mp

bimap_nn_get' :: (Ord b) => b -> Bimap_nn a b -> Maybe (Set a)
bimap_nn_get' b mp = map_get b # _bimapr_nn mp

bimap_nn_get_dflt :: (Ord a) => a -> Bimap_nn a b -> Set b
bimap_nn_get_dflt a mp = maybe_to_dflt # bimap_nn_get a mp

bimap_nn_get_dflt' :: (Ord b) => b -> Bimap_nn a b -> Set a
bimap_nn_get_dflt' b mp = maybe_to_dflt # bimap_nn_get' b mp

bimap_keys :: (Ord a) => Bimap a b -> [a]
bimap_keys = Map.keys . _bimapl

bimap_keys' :: (Ord b) => Bimap a b -> [b]
bimap_keys' = Map.keys . _bimapr

-----

bimap_from_list :: (BimapC mp, Ord a, Ord b) => [(a, b)] -> mp a b
bimap_from_list = foldr (uncurry bimap_insert) bimap_empty

bimap_from_list_set :: (BimapC mp, Ord a, Ord b) => [(a, Set b)] -> mp a b
bimap_from_list_set xs = bimap_from_list # do
  (a, set) <- xs
  b <- Set.toList set
  return (a, b)

bimap_from_map :: (BimapC mp, Ord a, Ord b) => Map a b -> mp a b
bimap_from_map = bimap_from_list . Map.toList

bimap_from_map_set :: (BimapC mp, Ord a, Ord b) => Map a (Set b) -> mp a b
bimap_from_map_set = bimap_from_list_set . Map.toList

type BimapOrdT a b = (Serializable a, Serializable b, Ord a, Ord b)

instance BimapOrdT a b => Serializable (Bimap a b) where
  ser = ser . _bimapl
  dser = fmap bimap_from_map dser

instance BimapOrdT a b => Serializable (Bimap_1n a b) where
  ser = ser . _bimapl_1n
  dser = fmap bimap_from_map_set dser

instance BimapOrdT a b => Serializable (Bimap_nn a b) where
  ser = ser . _bimapl_nn
  dser = fmap bimap_from_map_set dser
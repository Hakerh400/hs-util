module Tree where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import Data.Functor.Identity
import Control.Applicative
import Control.Monad

import Util
import Serializer

data Tree a
  = Leaf
  | Node a (Tree a) (Tree a)
  deriving (Eq, Ord, Show)

ser_tree :: (Serializable a) => Tree a -> Ser ()
ser_tree t = case t of
  Leaf -> ser_0
  Node a left right ->
    ser_1 >> ser a >> ser left >> ser right

dser_tree :: (Serializable a) => Dser (Tree a)
dser_tree = dser_bit >>= \b -> case b of
  0 -> pure Leaf
  1 -> do
    a <- dser
    left <- dser
    right <- dser
    return # Node a left right
  _ -> error ""

instance (Serializable a) => Serializable (Tree a) where
  ser = ser_tree
  dser = dser_tree

tree_to_list :: ([a] -> [a] -> ([[a]], [[a]])) -> Tree a -> [a]
tree_to_list f t = case t of
  Leaf -> []
  Node a left right -> let
    xs = tree_to_list f left
    ys = tree_to_list f right
    (x, y) = f xs ys
    in concat x ++ a : concat y

tree_empty :: Tree a
tree_empty = Leaf

tree_singleton :: a -> Tree a
tree_singleton a = Node a Leaf Leaf

tree_map :: (a -> b) -> Tree a -> Tree b
tree_map f tree = case tree of
  Leaf -> Leaf
  Node a left right -> Node (f a)
    (tree_map f left) (tree_map f right)

instance Functor Tree where
  fmap = tree_map

preorder :: Tree a -> [a]
preorder = tree_to_list # \a b -> ([], [a, b])

inorder :: Tree a -> [a]
inorder = tree_to_list # \a b -> ([a], [b])

postorder :: Tree a -> [a]
postorder = tree_to_list # \a b -> ([a, b], [])

show_tree :: String -> (a -> String) -> Tree a -> String
show_tree z f tree = case tree of
  Leaf -> z
  Node a left right -> put_in_parens # intercalate ", "
    [f a, show_tree z f left, show_tree z f right]

show_tree_m :: (Monad m) => String -> (a -> m String) -> Tree a -> m String
show_tree_m z f tree = case tree of
  Leaf -> pure z
  Node a left right -> do
    xs <- sequence [f a, show_tree_m z f left, show_tree_m z f right]
    return # put_in_parens # intercalate ", " xs
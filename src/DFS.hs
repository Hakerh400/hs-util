module DFS where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import Data.Foldable
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Cont

import Util

newtype DFS r m a = DFS
  { _dfs :: ContT r m a
  } deriving (Functor, Applicative, Monad)

instance MonadTrans (DFS r) where
  lift f = DFS # lift f

instance (Try m) => MonadFail (DFS r m) where
  fail s = DFS # ContT # const # fail s

instance (Try m, MonadIO m) => MonadIO (DFS r m) where
  liftIO m = DFS # liftIO m

dfs_run :: (Try m) => DFS r m r -> m (Maybe r)
dfs_run (DFS m) = try # evalContT m

dfs_choose :: (Try m) => [a] -> DFS r m a
dfs_choose xs = DFS # ContT # \fn -> do
  let ms = map fn xs
  foldr (<|>) (fail "") ms

dfs_choose_m :: (Try m) => [DFS r m a] -> DFS r m a
dfs_choose_m ms = do
  let n = len ms
  i <- dfs_choose # ico n
  Just fn <- pure # list_get i ms
  fn

dfs_try :: (Try m) => DFS a m a -> DFS r m (Maybe a)
dfs_try m = lift # dfs_run m
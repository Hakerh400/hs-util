{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE IncoherentInstances #-}
{-# LANGUAGE MonoLocalBinds #-}

module Util where

import Prelude hiding (print, putStrLn, getLine)
import qualified Prelude

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import Data.Bifunctor
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import System.IO.Unsafe
import GHC.Stack

infixr 0 #
(#) = id

infixr 1 <~
(<~) = flip

type N = Integer
type Prop = Bool
type Name = String
type Byte = Word8
type Buf = BS.ByteString
type HCS = HasCallStack

instance MonadFail Identity where
  fail = error

-- escape_char :: Char -> String
-- escape_char c = '.' : show (ord c)
-- 
-- escape_str :: String -> String
-- escape_str str = str --str >>= escape_char
-- 
-- putStr :: String -> IO ()
-- putStr str = Prelude.putStr # escape_str str
-- 
-- putStrLn :: String -> IO ()
-- putStrLn str = putStr # str ++ "\n"
-- 
-- print :: (Show a) => a -> IO ()
-- print a = putStrLn # show a

tab_size :: N
tab_size = 2

tab :: String
tab = replicate (fi tab_size) ' '

indent :: N -> String -> String
indent n str = concat (replicate (fi n) tab) ++ str

indent' :: N -> String
indent' n = indent n ""

put_in' :: a -> a -> [a] -> [a]
put_in' a b xs = [a] ++ xs ++ [b]

put_in :: [a] -> [a] -> [a] -> [a]
put_in a b xs = a ++ xs ++ b

put_in2 :: [a] -> [a] -> [a]
put_in2 a xs = put_in a a xs

put_in_list :: [[a]] -> [a] -> [a]
put_in_list list xs = concat
  [concat list, xs, concat # reverse list]

put_in_parens :: String -> String
put_in_parens = put_in' '(' ')'

put_in_brackets :: String -> String
put_in_brackets = put_in' '[' ']'

put_in_braces :: String -> String
put_in_braces = put_in' '{' '}'

put_in_parens' :: Prop -> String -> String
put_in_parens' p s = if p then put_in_parens s else s

fi :: (Integral a, Num b) => a -> b
fi = fromIntegral

unlines' :: [String] -> String
unlines' = intercalate "\n"

dbg :: (Show a) => a -> b
dbg a = trace' a # error ""

allM :: (Monad m) => (a -> m Prop) -> [a] -> m Prop
allM f = foldM (allM' f) True

allM' :: (Monad m) => (a -> m Prop) -> Prop -> a -> m Prop
allM' f p x = if p then f x else pure False

set_at :: N -> a -> [a] -> [a]
set_at n z xs = case xs of
  [] -> error ""
  (x : xs) -> if n == 0 then z : xs else
    x : set_at (n - 1) z xs

modify_at :: N -> (a -> a) -> [a] -> [a]
modify_at n f xs = case xs of
  [] -> error ""
  (x : xs) -> if n == 0 then f x : xs else
    x : modify_at (n - 1) f xs

insert_at :: N -> a -> [a] -> [a]
insert_at n z xs = if n == 0 then z : xs else case xs of
  [] -> error ""
  (x : xs) -> x : insert_at (n - 1) z xs

append_at :: N -> [a] -> [a] -> [a]
append_at n zs xs = if n == 0 then zs ++ xs else case xs of
  [] -> error ""
  (x : xs) -> x : append_at (n - 1) zs xs

remove_at :: N -> [a] -> [a]
remove_at n xs = case xs of
  [] -> error ""
  (x : xs) -> if n == 0 then xs else
    x : remove_at (n - 1) xs

list_move :: N -> N -> [a] -> [a]
list_move i j xs = let
  (xs1, x : xs2) = splitAt (fi i) xs
  ys = xs1 ++ xs2
  (ys1, ys2) = splitAt (fi j) ys
  in ys1 ++ x : ys2

ite :: Prop -> a -> a -> a
ite True a b = a
ite False a b = b

ite' :: Prop -> a -> a -> a
ite' False a b = a
ite' True a b = b

-- assert :: (Monad m) => Prop -> m ()
-- assert True = pure ()

swap :: (a, b) -> (b, a)
swap (a, b) = (b, a)

list_swap :: N -> N -> [a] -> [a]
list_swap i j xs = set_at i (xs !! fi j) # set_at j (xs !! fi i) xs

liftm :: (Monad m) => (a -> b) -> a -> m b
liftm f x = pure # f x

liftm2 :: (Monad m) => (a -> b -> c) -> a -> b -> m c
liftm2 f x y = pure # f x y

set_insert :: (Ord k) => k -> Set k -> Set k
set_insert = Set.insert

set_insert_new :: HCS => (Ord k) => k -> Set k -> Set k
set_insert_new k set = if Set.member k set
  then error ""
  else Set.insert k set

set_insert' :: (Ord k, MonadError e m) => k -> Set k -> e -> m (Set k)
set_insert' k set msg = if Set.member k set
  then raise msg
  else pure # set_insert k set

map_insert :: (Ord k) => k -> v -> Map k v -> Map k v
map_insert = Map.insert

map_insert_new :: HCS => (Ord k) => k -> v -> Map k v -> Map k v
map_insert_new k v mp = if Map.member k mp
  then error ""
  else Map.insert k v mp

map_insert' :: (Ord k, MonadError e m) => k -> v -> Map k v -> e -> m (Map k v)
map_insert' k v mp msg = if Map.member k mp
  then raise msg
  else pure # map_insert k v mp

get_avail :: (Ord a) => [a] -> Set a -> a
get_avail xs s = case xs of
  [] -> error ""
  (x : xs) -> if Set.member x s then get_avail xs s else x

get_avail' :: (Ord a) => [a] -> Set a -> (a, Set a)
get_avail' xs s = let
  x = get_avail xs s
  in (x, set_insert x s)

nop' :: (Inhabited a, Monad m) => m a
nop' = pure dflt

nop :: (Monad m) => m ()
nop = pure ()

fix :: (a -> a) -> a
fix f = f (fix f)

space :: Char
space = ' '

lift_idemp :: (a -> a) -> (a -> a) -> a -> a
lift_idemp tr f = tr . f . tr

reversed :: ([a] -> [a]) -> [a] -> [a]
reversed = lift_idemp reverse

trim_left' :: (Eq a) => a -> [a] -> [a]
trim_left' x = dropWhile (x==)

trim_right' :: (Eq a) => a -> [a] -> [a]
trim_right' x = reversed # trim_left' x

trim' :: (Eq a) => a -> [a] -> [a]
trim' x = trim_right' x . trim_left' x

trim_left :: String -> String
trim_left = trim_left' space

trim_right :: String -> String
trim_right = trim_right' space

trim :: String -> String
trim = trim' space

split_adv' :: ([a] -> [a] -> Maybe N) -> N -> [a] -> [a] -> [a] -> [[a]] -> [[a]]
split_adv' func skip prev list sub acc = case list of
  [] -> case func prev list of
    Nothing -> reverse (reverse sub : acc)
    _ -> reverse ([] : reverse sub : acc)
  (x:xs) -> case skip of
    0 -> case func prev list of
      Nothing -> split_adv' func 0 (x : prev) xs (x : sub) acc
      Just len -> case len of
        0 -> split_adv' func 0 (x : prev) xs (x : []) (reverse sub : acc)
        n -> split_adv' func (n - 1) (x : prev) xs [] (reverse sub : acc)
    n -> split_adv' func (n - 1) (x : prev) xs sub acc

split_adv :: ([a] -> [a] -> Maybe N) -> [a] -> [[a]]
split_adv func list = split_adv' func 0 [] list [] []

split_at_elem :: (Eq a) => a -> [a] -> [[a]]
split_at_elem z = split_adv # \xs ys -> case ys of
  [] -> Nothing
  (y:ys) -> ite (y == z) (Just 1) Nothing

split_at_list :: (Eq a) => [a] -> [a] -> [[a]]
split_at_list zs = let
  n = length zs
  jn = Just # fi n
  in split_adv # \xs ys -> let
    (ys1, ys2) = splitAt n ys
    in if ys1 == zs then jn else Nothing

nat_find :: (N -> Prop) -> N
nat_find p = nat_find' p 0

nat_find' :: (N -> Prop) -> N -> N
nat_find' p n = ite (p n) n # nat_find' p (n + 1)

guard' :: (Monad m) => Prop -> String -> m ()
guard' val msg = ite val (pure ()) (error msg)

nodup :: (Eq a) => [a] -> Prop
nodup xs = length xs == length (nub xs)

show_list :: (a -> String) -> [a] -> String
show_list f xs = put_in_brackets # intercalate ", " # map f xs

show_list' :: (a -> String) -> [a] -> String
show_list' f xs = case xs of
  [a] -> f a
  _ -> show_list f xs

show_list_m :: (Monad m) => (a -> m String) -> [a] -> m String
show_list_m f xs = do
  xs <- mapM f xs
  return # put_in_brackets # intercalate ", " xs

show_set :: (a -> String) -> Set a -> String
show_set f xs = put_in_braces # intercalate ", " # map f # Set.toList xs

-- paragraphs_aux :: [a] -> [a] -> [a]
-- paragraphs_aux s x = ite (null x) [] (s ++ x)
-- 
-- paragraphs' :: [a] -> [[a]] -> [a]
-- paragraphs' s [] = []
-- paragraphs' s (x:xs) = x ++ (xs >>= paragraphs_aux s)

-- paragraphs :: [String] -> String
-- paragraphs = paragraphs' "\n\n"

paragraphs :: [String] -> String
paragraphs xs = intercalate "\n\n" # filter (not . null) xs

len :: (Integral n) => [a] -> n
len xs = fi # length xs

-- list_get' :: N -> [a] -> Maybe a
-- list_get' _ [] = Nothing
-- list_get' 0 (x:xs) = Just x
-- list_get' i (x:xs) = list_get' (i - 1) xs
-- 
-- list_get :: N -> [a] -> a
-- list_get i xs = fromJust # list_get' i xs

list_get :: N -> [a] -> Maybe a
list_get i xs = case xs of
  [] -> Nothing
  (x : xs) -> if i == 0 then pure x else
    list_get (i - 1) xs

-- map_get' :: (Ord k) => k -> Map k v -> Maybe v
-- map_get' = Map.lookup
-- 
-- map_get :: (Ord k) => k -> Map k v -> v
-- map_get k mp = case map_get' k mp of
--   Nothing -> error ""
--   Just v -> v

map_get :: (Ord k) => k -> Map k v -> Maybe v
map_get = Map.lookup

sanl :: String -> [String]
sanl = split_adv f where
  f _ ('\r':'\n':_) = Just 2
  f _ ('\r':_) = Just 1
  f _ ('\n':_) = Just 1
  f _ _ = Nothing

sanll :: String -> [String]
sanll = split_adv f where
  f _ ('\r':'\n':'\r':'\n':_) = Just 4
  f _ ('\r':'\n':'\r':_) = Just 3
  f _ ('\r':'\n':'\n':_) = Just 3
  f _ ('\r':'\r':'\n':_) = Just 3
  f _ ('\r':'\r':_) = Just 2
  f _ ('\n':'\r':'\n':_) = Just 3
  f _ ('\n':'\r':_) = Just 2
  f _ ('\n':'\n':_) = Just 2
  f _ _ = Nothing

show_maybe :: (Show a) => Maybe a -> String
show_maybe = maybe "/" show

is_letter :: Char -> Prop
is_letter c = let c1 = toLower c in c1 >= 'a' && c1 <= 'z'

is_digit :: Char -> Prop
is_digit = isDigit

is_sub_digit :: Char -> Prop
is_sub_digit c = c >= '₀' && c <= '₉'

greek_alpha_low :: Char
greek_alpha_low = '\945'

greek_omega_low :: Char
greek_omega_low = '\969'

is_greek :: Char -> Prop
is_greek c = let
  c1 = toLower c
  in c1 >= greek_alpha_low && c1 <= greek_omega_low

greek_letters_low :: String
greek_letters_low = [greek_alpha_low .. greek_omega_low]

mapi :: (N -> a -> b) -> [a] -> [b]
mapi f = zipWith f [0..]

show_either :: (a -> String) -> (b -> String) -> Either a b -> String
show_either f g a = case a of
  Left a -> f a
  Right a -> g a

show_either' :: (Show a, Show b) => Either a b -> String
show_either' = show_either show show

unsnoc :: [a] -> ([a], a)
unsnoc xs = (init xs, last xs)

from_left :: Either a b -> a
from_left a = case a of
  Left a -> a
  _ -> error ""

from_right :: Either a b -> b
from_right a = case a of
  Right a -> a
  _ -> error ""

mk_pair :: a -> b -> (a, b)
mk_pair a b = (a, b)

mk_pair' :: b -> a -> (a, b)
mk_pair' = flip mk_pair

usc' :: Char
usc' = '\x5F'

usc :: String
usc = [usc']

either_to_maybe :: Either a b -> Maybe b
either_to_maybe e = case e of
  Left _ -> Nothing
  Right a -> Just a

find_indices :: (a -> Prop) -> [a] -> [N]
find_indices f xs = map snd # filter fst # mapi (\i x -> (f x, i)) xs

mk_list' :: (N -> a) -> N -> N -> [a]
mk_list' f n i = if i == n then [] else
  f i : mk_list' f n (i + 1)

mk_list :: N -> (N -> a) -> [a]
mk_list n f = mk_list' f n 0

scanM :: (Monad m) => (b -> a -> m b) -> b -> [a] -> m [b]
scanM f z xs = case xs of
  [] -> pure []
  (x:xs) -> do
    y <- f z x
    ys <- scanM f y xs
    return # y : ys

for :: (a -> a) -> a -> N -> a
for f z n = if n == 0 then z else f # for f z # n - 1

seq_to_list' :: (N -> a) -> N -> [a]
seq_to_list' f n = f n : seq_to_list' f (n + 1)

seq_to_list :: (N -> a) -> [a]
seq_to_list f = seq_to_list' f 0

get_avail_of_set' :: (Ord a) => [a] -> Set a -> a
get_avail_of_set' xs set = case xs of
  [] -> error ""
  (x : xs) -> if Set.member x set
    then get_avail_of_set' xs set else x

get_avail_of_set :: (Ord a) => (N -> a) -> Set a -> a
get_avail_of_set f set = get_avail_of_set' (seq_to_list f) set

cons_head :: a -> [[a]] -> [[a]]
cons_head x ys = case ys of
  [] -> error ""
  (y : ys) -> (x : y) : ys

pad_start :: N -> a -> [a] -> [a]
pad_start n z xs = let n' = len xs in
  if n' < n then replicate (fi # n - n') z ++ xs else xs

pad_end :: N -> a -> [a] -> [a]
pad_end n z xs = let n' = len xs in
  if n' < n then xs ++ replicate (fi # n - n') z else xs

split_into_groups :: [N] -> [a] -> [[a]]
split_into_groups ns xs = case ns of
  [] -> if null xs then [] else undefined
  (n:ns) -> if n == 0
    then [] : split_into_groups ns xs
    else case xs of
      [] -> undefined
      (x:xs) -> cons_head x # split_into_groups (n - 1 : ns) xs

split_into_same_groups' :: ([a] -> b) -> N -> [a] -> [b]
split_into_same_groups' f n xs = if null xs then [] else
  case splitAt (fi n) xs of
    (xs, ys) -> f xs : split_into_same_groups' f n ys

split_into_same_groups :: N -> [a] -> [[a]]
split_into_same_groups = split_into_same_groups' id

split_into_same_groups_pad_start :: N -> a -> [a] -> [[a]]
split_into_same_groups_pad_start n z xs =
  split_into_same_groups' (pad_start n z) n xs

split_into_same_groups_pad_end :: N -> a -> [a] -> [[a]]
split_into_same_groups_pad_end n z xs =
  split_into_same_groups' (pad_end n z) n xs

iter_st :: (a -> (b, a)) -> a -> [b]
iter_st f z = let (x, y) = f z in
  x : iter_st f y

iter_st_until :: (a -> Prop) -> (a -> (b, a)) -> a -> [b]
iter_st_until p f z = if p z then [] else let (x, y) = f z in
  x : iter_st_until p f y

common_prefix :: (Eq a) => [a] -> [a] -> ([a], [a], [a])
common_prefix xs0@(x:xs) ys0@(y:ys) = if x == y
  then let (a, b, c) = common_prefix xs ys in (x : a, b, c)
  else ([], xs0, ys0)
common_prefix xs ys = ([], xs, ys)

raise :: (MonadError e m) => e -> m a
raise = throwError

-- try :: (MonadError e m) => m a -> m (Either e a)
-- try m = catchError (m >>= liftm Right) # liftm Left

maximum' :: (Num a, Ord a) => [a] -> a
maximum' xs = maximum # 0 : xs

fst_just' :: Maybe a -> Maybe a -> Maybe a
fst_just' m1 m2 = case m1 of
  Nothing -> m2
  _ -> m1

fst_just :: [Maybe a] -> Maybe a
fst_just xs = case xs of
  [] -> Nothing
  (x:xs) -> case x of
    Just _ -> x
    Nothing -> fst_just xs

equiv_classes' :: (a -> a -> Prop) -> [a] -> [[a]]
equiv_classes' f xs = case xs of
  [] -> []
  (x:xs) -> let
    cs = equiv_classes' f xs
    (cs1, cs2) = partition (any # f x) cs
    in (x : concat cs1) : cs2

equiv_classes :: (Ord a) => (a -> a -> Prop) -> Set a -> Set (Set a)
equiv_classes f set = Set.fromList # map Set.fromList #
  equiv_classes' f # Set.toList set

head' :: a -> [a] -> a
head' z xs = case xs of
  [] -> z
  (x : _) -> x

last' :: a -> [a] -> a
last' z xs = if null xs then z else last xs

tail' :: [a] -> [a]
tail' xs = case xs of
  [] -> []
  (_ : xs) -> xs

init' :: [a] -> [a]
init' xs = if null xs then [] else init xs

headm :: [a] -> Maybe a
headm xs = case xs of
  [] -> Nothing
  (x:_) -> Just x

lastm :: [a] -> Maybe a
lastm xs = if null xs then Nothing else Just # last xs

-- set_to_map :: (Ord k, Ord a) => (a -> k) -> Set a -> Map k a
-- set_to_map f set = Map.fromList # map (\a -> (f a, a)) # Set.toList set

set_fst' :: (Ord a) => Set a -> a
set_fst' = Set.elemAt 0

set_fst :: (Ord a) => Set a -> Maybe a
set_fst set = if Set.null set then Nothing else
  Just # set_fst' set

map_fst' :: (Ord k) => Map k a -> (k, a)
map_fst' = Map.elemAt 0

map_fst :: (Ord k) => Map k a -> Maybe (k, a)
map_fst mp = if Map.null mp then Nothing else
  Just # map_fst' mp

map_fst_key' :: (Ord k) => Map k a -> k
map_fst_key' = fst . Map.elemAt 0

map_fst_key :: (Ord k) => Map k a -> Maybe k
map_fst_key mp = if Map.null mp then Nothing else
  Just # map_fst_key' mp

map_fst_val' :: (Ord k) => Map k a -> a
map_fst_val' = snd . Map.elemAt 0

map_fst_val :: (Ord k) => Map k a -> Maybe a
map_fst_val mp = if Map.null mp then Nothing else
  Just # map_fst_val' mp

remove_empty_lists :: [[a]] -> [[a]]
remove_empty_lists = filter # not . null

is_line_empty :: String -> Prop
is_line_empty line = case line of
  "" -> True
  (' ':line) -> is_line_empty line
  _ -> False

remove_empty_lines :: String -> String
remove_empty_lines str = unlines' # filter (not . is_line_empty) # sanl str

trim_start :: (Eq a) => a -> [a] -> [a]
trim_start z xs = case xs of
  [] -> []
  (x:xs') -> if x == z then trim_start z xs' else xs

trim_end :: (Eq a) => a -> [a] -> [a]
trim_end z xs = case xs of
  [] -> []
  (x:xs') -> case trim_end z xs' of
    [] -> if x == z then [] else [x]
    ys -> x : ys

class ShallowOrd a where
  shallow_compare :: a -> a -> Ordering

mk_sorted_pair :: (Ord a) => a -> a -> (a, a)
mk_sorted_pair a b = if compare a b /= GT then (a, b) else (b, a)

mk_shallow_sorted_pair :: (ShallowOrd a) => a -> a -> (a, a)
mk_shallow_sorted_pair a b = if shallow_compare a b /= GT then (a, b) else (b, a)

newtype Lit = Lit [String]

instance Show Lit where
  show (Lit xs) = concat xs

class Inhabited a where
  dflt :: a

instance Inhabited () where
  dflt = ()

instance (Inhabited a, Inhabited b) => Inhabited (a, b) where
  dflt = (dflt, dflt)

instance Inhabited (Maybe a) where
  dflt = Nothing

instance Inhabited [a] where
  dflt = []

instance Inhabited (Set a) where
  dflt = Set.empty

instance Inhabited (Map a b) where
  dflt = Map.empty

instance (Inhabited a, Monad m) => Inhabited (StateT s m a) where
  dflt = pure dflt

instance Inhabited Prop where
  dflt = False

instance Inhabited Int where
  dflt = 0

instance Inhabited N where
  dflt = 0

from_maybe :: (Inhabited a) => Maybe a -> a
from_maybe = fromMaybe dflt

show_map' :: (Ord k) => (k -> String) -> (v -> String) -> k -> v -> (String, String)
show_map' f g k v = (f k, g v)

show_map :: (Ord k) => (k -> String) -> (v -> String) -> Map k v -> String
show_map f g mp = let
  ps = map (uncurry # show_map' f g) # Map.toList mp
  n = maximum' # map (len . fst) ps
  in unlines' # flip map ps # \(sk, sv) -> concat
    [pad_end n space sk, " ---> ", sv]

trace_str :: String -> a -> a
trace_str str a = unsafePerformIO # do
  putStrLn str
  return a

trace_show :: (Show a) => a -> b -> b
trace_show a b = unsafePerformIO # do
  print a
  return b

trace' :: (Show a) => a -> b -> b
trace' = trace_show

trace :: (Show a) => a -> a
trace a = trace' a a

tracem :: (Monad m, Show a) => a -> m ()
tracem a = do
  () <- trace' a nop
  nop

tracem' :: (Monad m, Show a) => m a -> m a
tracem' m = do
  a <- m
  tracem a
  return a

trace_with :: (a -> String) -> a -> a
trace_with f a = trace_str (f a) a

show_stack' :: CallStack -> String
show_stack' stack = unlines' # drop 1 #
  map <~ getCallStack stack # \(_, loc) -> let
    file = srcLocFile loc
    line = srcLocStartLine loc
    in concat [file, ":", show line]

show_stack :: HCS => String
show_stack = show_stack' callStack

trace_stack :: (HCS, Monad m) => m ()
trace_stack = tracem # Lit [show_stack]

print_stack :: HCS => IO ()
print_stack = putStrLn show_stack

replace_with_map :: (Ord a) => Map a a -> a -> a
replace_with_map mp a = Map.findWithDefault a a mp

replace_with_map_in_list :: (Ord a) => Map a a -> [a] -> [a]
replace_with_map_in_list mp xs = map (replace_with_map mp) xs

snd_just :: Maybe a -> Maybe a -> Maybe a
snd_just m1 m2 = case m2 of
  Nothing -> m1
  _ -> m2

maybe_to_list :: Maybe a -> [a]
maybe_to_list x = case x of
  Nothing -> []
  Just x -> [x]

list_to_maybe :: [a] -> Maybe a
list_to_maybe xs = case xs of
  [] -> Nothing
  (x : _) -> Just x

flip_pair :: (a, b) -> (b, a)
flip_pair (a, b) = (b, a)

flip_map :: (Ord a, Ord b) => Map a b -> Map b a
flip_map mp = Map.fromList # map flip_pair # Map.toList mp

quote :: String -> String
quote = put_in2 "\""

show_name :: Name -> String
show_name = quote

lift2 :: (MonadTrans t1, MonadTrans t2, Monad m, Monad (t2 m)) =>
  m a -> t1 (t2 m) a
lift2 = lift . lift

-- starts_with :: (Eq a) => [a] -> [a] -> Prop
-- starts_with [] _ = True
-- starts_with (x:xs) (y:ys) = x == y && starts_with xs ys
-- starts_with _ _ = False

list_to_map :: [a] -> Map N a
list_to_map xs = Map.fromList # zip [0..] xs

list_to_map_inv :: (Ord a) => [a] -> Map a N
list_to_map_inv xs = Map.fromList # zip xs [0..]

map_insert_list :: (Ord k) => k -> v -> Map k [v] -> Map k [v]
map_insert_list k v mp = let
  vs = v : fromMaybe [] (Map.lookup k mp)
  in map_insert k vs mp

list_to_map_list :: (Ord k) => [(k, v)] -> Map k [v]
list_to_map_list xs = case xs of
  [] -> Map.empty
  ((k, v) : xs) -> map_insert_list k v #
    list_to_map_list xs

snoc :: [a] -> a -> [a]
snoc xs x = xs ++ [x]

snoc' :: a -> [a] -> [a]
snoc' = flip snoc

list_rot_left :: [a] -> [a]
list_rot_left xs = case xs of
  [] -> error ""
  (x : xs) -> snoc xs x

list_rot_right :: [a] -> [a]
list_rot_right xs = last xs : init xs

flip2 :: (a -> b -> c -> d) -> b -> c -> a -> d
flip2 f a b c = f c a b

range :: N -> [N]
range n = [0 .. n - 1]

ord_to_dif :: Ordering -> N
ord_to_dif ord = case ord of
  LT -> -1
  EQ -> 0
  GT -> 1

digits :: [Char]
digits = ['0'..'9']

letters_low :: [Char]
letters_low = ['a'..'z']

letters_up :: [Char]
letters_up = ['A'..'Z']

letters :: [Char]
letters = letters_low ++ letters_up

all_combs :: [a] -> [[a]]
all_combs xs = [] : do
  ys <- all_combs xs
  x <- xs
  return # snoc ys x

all_combs' :: [a] -> [[a]]
all_combs' xs = tail' # all_combs xs

mk_combs :: [[a]] -> [[a]]
mk_combs xs = case xs of
  [] -> [[]]
  (x:xs) -> do
    y <- x
    ys <- mk_combs xs
    return # y : ys

mk_avail_getter' :: (Ord a) => Set a -> [a] -> a
mk_avail_getter' set xs = case xs of
  [] -> error ""
  (x : xs) -> if Set.member x set
    then mk_avail_getter' set xs else x

mk_avail_getter :: (Ord a) => [a] -> Set a -> (a, Set a)
mk_avail_getter xs set = let
  x = mk_avail_getter' set xs
  in (x, set_insert x set)

set_size :: (Ord v, Integral n) => Set v -> n
set_size s = fi # Set.size s

map_size :: (Ord k, Integral n) => Map k v -> n
map_size mp = fi # Map.size mp

pair_map_fst :: (a -> c) -> (a, b) -> (c, b)
pair_map_fst f (a, b) = (f a, b)

pair_map_snd :: (b -> c) -> (a, b) -> (a, c)
pair_map_snd f (a, b) = (a, f b)

pair_map :: (a -> c) -> (b -> d) -> (a, b) -> (c, d)
pair_map f g (a, b) = (f a, g b)

pair_map' :: (a -> b) -> (a, a) -> (b, b)
pair_map' f = pair_map f f

pair_map_rev :: (b -> d) -> (a -> c) -> (a, b) -> (c, d)
pair_map_rev = flip pair_map

pair_map_fst_m :: (Monad m) => (a -> m c) -> (a, b) -> m (c, b)
pair_map_fst_m f (a, b) = f a >>= \a -> pure (a, b)

pair_map_snd_m :: (Monad m) => (b -> m c) -> (a, b) -> m (a, c)
pair_map_snd_m f (a, b) = f b >>= \b -> pure (a, b)

pair_map_m :: (Monad m) => (a -> m c) -> (b -> m d) -> (a, b) -> m (c, d)
pair_map_m f g (a, b) = f a >>= \a -> g b >>= \b -> pure (a, b)

either_map :: (a -> c) -> (b -> d) -> Either a b -> Either c d
either_map f g x = case x of
  Left a -> Left # f a
  Right b -> Right # g b

either_map_m :: (Monad m) => (a -> m c) -> (b -> m d) ->
  Either a b -> m (Either c d)
either_map_m f g x = case x of
  Left a -> f a >>= liftm Left
  Right b -> g b >>= liftm Right

-- logb' :: (MonadError e m) => Char -> m ()
-- logb' c = tracem # Lit [put_in2 "\n" # replicate 100 c]
-- 
-- logb :: (MonadError e m) => m ()
-- logb = logb' '='

maybe_to_pair :: Maybe a -> (Prop, a)
maybe_to_pair m = case m of
  Just a -> (True, a)
  Nothing -> (False, undefined)

set_to_map_unit :: (Ord a) => Set a -> Map a ()
set_to_map_unit s = Map.fromList # map (mk_pair' ()) # Set.toList s

-- class (Monad m) => VarCnt m where
--   cnt_next :: m N

map_push :: a -> Map N a -> Map N a
map_push x mp = map_insert (map_size mp) x mp

map_push_inv :: (Ord a) => a -> Map a N -> Map a N
map_push_inv x mp = map_insert x (map_size mp) mp

maybe_either :: Maybe a -> Maybe b -> Maybe (Either a b)
maybe_either ma mb = case ma of
  Just a -> Just (Left a)
  Nothing -> case mb of
    Just b -> Just (Right b)
    Nothing -> Nothing

map_inv :: (Ord a, Ord b) => Map a b -> Map b a
map_inv mp = Map.fromList # map swap # Map.toList mp

map_dif_set :: (Ord k) => Map k v -> Set k -> Map k v
map_dif_set mp s = foldr Map.delete mp # Set.toList s

list_dif :: (Eq a) => [a] -> [a] -> [a]
list_dif xs ys = filter (\x -> not # elem x ys) xs

-----

class (MonadFail m) => Try m where
  try :: m a -> m (Maybe a)

instance Try Maybe where
  try = Just

instance (MonadFail m, Try m) => Try (StateT s m) where
  try (StateT f) = StateT # \s -> do
    res <- try # f s
    return # case res of
      Nothing -> (Nothing, s)
      Just (a, s) -> (Just a, s)

instance (MonadFail m, Try m) => Try (ExceptT e m) where
  try (ExceptT f) = ExceptT # do
    res <- try f
    return # case res of
      Nothing -> Right Nothing
      Just a -> fmap Just a

infixr 5 <|>

-- class OrElse a where
--   (<|>) :: a -> a -> a

(<|>) :: (Try m) => m a -> m a -> m a
f <|> g = do
  res <- try f
  maybe g pure res

-- instance (MonadFail m, forall a. OrElse (m a)) => Try m where
--   try m = fmap Just m <|> pure Nothing

-- instance (Try m) => OrElse (m a) where
--   f <|> g = do
--     res <- try f
--     case res of
--       Nothing -> g
--       Just a -> pure a

-- instance (OrElse m) => OrElse (StateT s m) where
--   f <|> g = StateT # \s -> runStateT f s <|> runStateT g s

try_list :: (Try m) => [m a] -> m (Maybe a)
try_list ms = case ms of
  [] -> pure Nothing
  (m : ms) -> do
    res <- try m
    case res of
      Just a -> pure # pure a
      Nothing -> try_list ms

-----

digit_zero_n :: N
digit_zero_n = 48

sub_digit_zero_n :: N
sub_digit_zero_n = 8320

digit_to_char :: N -> Char
digit_to_char d = chr # fi # digit_zero_n + d

char_to_digit :: Char -> N
char_to_digit c = fi (ord c) - digit_zero_n

sub_digits :: String
sub_digits = mk_list 10 # \i -> chr # fi # sub_digit_zero_n + i

digit_to_sub :: N -> Char
digit_to_sub d = chr # fi # sub_digit_zero_n + d

sub_to_digit :: Char -> N
sub_to_digit c = fi (ord c) - sub_digit_zero_n

digit_char_to_sub :: Char -> Char
digit_char_to_sub c = digit_to_sub # char_to_digit c

sub_to_digit_char :: Char -> Char
sub_to_digit_char c = digit_to_char # sub_to_digit c

nat_to_digits' :: N -> [N]
nat_to_digits' n = if n == 0 then [] else
  mod n 10 : nat_to_digits' (div n 10)

nat_to_digits :: N -> [N]
nat_to_digits n = if n == 0 then [0] else reverse # nat_to_digits' n

digits_to_nat :: [N] -> N
digits_to_nat = flip foldl' 0 # \acc d -> acc * 10 + d

nat_to_sub :: N -> String
nat_to_sub n = map digit_to_sub # nat_to_digits n

sub_to_nat :: String -> N
sub_to_nat s = digits_to_nat # map sub_to_digit s

list_find_dup' :: (Eq a) => [a] -> [a] -> Maybe a
list_find_dup' xs ys = case xs of
  [] -> Nothing
  (x:xs) -> case ys of
    [] -> pure x
    (y:ys) -> if x /= y then pure x else
      list_find_dup' xs ys

list_find_dup :: (Eq a) => [a] -> Maybe a
list_find_dup xs = list_find_dup' xs # nub xs

inc :: (Num a) => a -> a
inc n = n + 1

dec :: (Num a) => a -> a
dec n = n - 1

if' :: (Inhabited a) => Prop -> a -> a
if' h a = ite h a dflt

ifm' :: (Inhabited a, Monad m) => Prop -> m a -> m a
ifm' p m = ite p m nop'

ifmn' :: (Inhabited a, Monad m) => Prop -> m a -> m a
ifmn' p = ifm' # not p

ifm :: (Monad m) => Prop -> m a -> m ()
ifm p m = ifm' p # void m

ifmn :: (Monad m) => Prop -> m a -> m ()
ifmn p m = ifmn' p # void m

ifu :: Prop -> a -> a
ifu p a = ite p a undefined

ifmu :: (Monad m) => Prop -> m a -> m a
ifmu p m = ite p m # pure undefined

is_printable :: Char -> Prop
is_printable c = c >= ' ' && c <= '~'

nat_base_digits :: String
nat_base_digits = digits ++ letters_low

nat_to_base' :: N -> N -> String
nat_to_base' b n = if n == 0 then "" else let
  (n', d) = divMod n b
  in fromJust (list_get d nat_base_digits) : nat_to_base' b n'

nat_to_base :: N -> N -> String
nat_to_base b n = if n == 0 then "0" else reverse # nat_to_base' b n

nat_to_hex :: N -> String
nat_to_hex = nat_to_base 16

prop_xor :: Prop -> Prop -> Prop
prop_xor = (/=)

prop_xors :: [Prop] -> Prop
prop_xors = foldr prop_xor False

iter :: (Integral n) => n -> a -> (a -> a) -> a
iter n z f = if n == 0 then z else iter (n - 1) (f z) f

iterM :: (Integral n, Monad m) => n -> a -> (a -> m a) -> m a
iterM n z f = if n == 0 then pure z else do
  z <- f z
  iterM (n - 1) z f

show_pair :: (a -> String) -> (b -> String) -> (a, b) -> String
show_pair f g (a, b) = put_in_parens # concat [f a, ", ", g b]

run :: Identity a -> a
run = runIdentity

sort_by' :: (a -> a -> Ordering) -> [a] -> [a] -> [a]
sort_by' f ys xs = if null xs then ys else
  case findIndex <~ xs # \x -> all <~ xs # \y -> f x y /= GT of
    Just i -> sort_by' f (xs !! i : ys) # remove_at (fi i) xs
    _ -> error ""

sort_by :: (a -> a -> Ordering) -> [a] -> [a]
sort_by f xs = reverse # sort_by' f [] xs

from_either_same :: Either a a -> a
from_either_same x = case x of
  Left a -> a
  Right b -> b

query :: (Monad m) => StateT s m a -> StateT s m a
query m = do
  s <- get
  a <- m
  put s
  return a

ifm_just :: (Monad m) => Maybe a -> (a -> m ()) -> m ()
ifm_just v f = case v of
  Nothing -> nop
  Just v -> f v

ifm_just_m :: (Monad m) => m (Maybe a) -> (a -> m ()) -> m ()
ifm_just_m m f = do
  v <- m
  ifm_just v f

min_b :: (Foldable t, Ord a, Bounded a) => t a -> a
min_b xs = if null xs then maxBound else minimum xs

max_b :: (Foldable t, Ord a, Bounded a) => t a -> a
max_b xs = if null xs then minBound else maximum xs

-- map_set_insert :: (Ord k, Ord v) => k -> v -> Map k (Set v) -> Map k (Set v)
-- map_set_insert k v = Map.alter <~ k # \s -> Just # case s of
--   Nothing -> Set.singleton v
--   Just s -> Set.insert v s
-- 
-- map_set_remove :: (Ord k, Ord v) => k -> v -> Map k (Set v) -> Map k (Set v)
-- map_set_remove k v = Map.alter <~ k # \s -> do
--   s <- s
--   s <- pure # Set.delete v s
--   False <- pure # Set.null s
--   return s

map_set_get :: (Ord k, Ord v) => k -> Map k (Set v) -> Set v
map_set_get = Map.findWithDefault Set.empty

swap_ite' :: Prop -> a -> a -> (a, a)
swap_ite' p a b = ite p (a, b) (b, a)

swap_ite :: (Monad m) => Prop -> a -> a -> m (a, a)
swap_ite p a b = pure # swap_ite' p a b

zip' :: [a] -> [b] -> [(a, b)]
zip' xs ys = case (xs, ys) of
  ([], []) -> []
  ((x : xs), (y : ys)) -> (x, y) : zip' xs ys
  _ -> error "zip"

zip_with' :: (a -> b -> c) -> [a] -> [b] -> [c]
zip_with' f xs ys = case (xs, ys) of
  ([], []) -> []
  ((x : xs), (y : ys)) -> f x y : zip_with' f xs ys
  _ -> error "zip_with"

zip_with :: [a] -> [b] -> (a -> b -> c) -> [c]
zip_with xs ys f = zip_with' f xs ys

fold_r :: [a] -> b -> (a -> b -> b) -> b
fold_r xs z f = foldr f z xs

fold_l :: [a] -> b -> (b -> a -> b) -> b
fold_l xs z f = foldl' f z xs

set_fold_r :: (Ord a) => Set a -> b -> (a -> b -> b) -> b
set_fold_r set z f = Set.foldr f z set

set_fold_l :: (Ord a) => Set a -> b -> (b -> a -> b) -> b
set_fold_l set z f = Set.foldl f z set

map_fold_r :: (Ord k) => Map k v -> b -> (k -> v -> b -> b) -> b
map_fold_r mp z f = Map.foldrWithKey f z mp

map_fold_l :: (Ord k) => Map k v -> b -> (b -> k -> v -> b) -> b
map_fold_l mp z f = Map.foldlWithKey f z mp

foldm_r :: (Monad m) => [a] -> b -> (a -> b -> m b) -> m b
foldm_r xs z f = foldrM f z xs

foldm_l :: (Monad m) => [a] -> b -> (b -> a -> m b) -> m b
foldm_l xs z f = foldlM f z xs

ico :: (Integral n) => n -> [n]
ico n = [0 .. n - 1]

-- set_disj_union' :: (Ord a) => Set a -> Set a -> Either a (Set a)
-- set_disj_union' set1 set2 = (\f -> Set.foldrM f (Just set2) set1) #
--   \x mset -> do
--     set <- mset
--     False <- pure # Set.member x set
--     return # Set.insert x set
-- 
-- set_disj_unions' :: (Ord a) => [Set a] -> Either a (Set a)
-- set_disj_unions' sets = fold_r sets (Just Set.empty) #
--   \set1 mset2 -> do
--     set2 <- mset2
--     set_disj_union' set1 set2
-- 
-- set_disj_union :: (Ord a) => Set a -> Set a -> Set a
-- set_disj_union set1 set2 = fromJust # set_disj_union' set1 set2
-- 
-- set_disj_unions :: (Ord a) => [Set a] -> Set a
-- set_disj_unions sets = fromJust # set_disj_unions' sets

map_disj_union' :: (Ord k) => Map k v -> Map k v -> Either k (Map k v)
map_disj_union' mp1 mp2 = foldm_r (Map.toList mp2) mp1 #
  \(k, v) mp -> do
    ifm (Map.member k mp) # Left k
    return # Map.insert k v mp

map_disj_unions' :: (Ord k) => [Map k v] -> Either k (Map k v)
map_disj_unions' mps = foldm_r mps Map.empty map_disj_union'

map_disj_union :: (Ord k) => Map k v -> Map k v -> Map k v
map_disj_union mp1 mp2 = from_right # map_disj_union' mp1 mp2

map_disj_unions :: (Ord k) => [Map k v] -> Map k v
map_disj_unions mps = from_right # map_disj_unions' mps

from_maybe_t :: (Monad m) => MaybeT m a -> m Prop
from_maybe_t m = do
  res <- runMaybeT m
  return # isJust res

map_map_with_key :: (Ord k, Ord k') => (k -> v -> (k', v')) -> Map k v -> Map k' v'
map_map_with_key f mp = Map.fromList # map <~ Map.toList mp # uncurry f

set_map_m :: (Monad m, Ord k, Ord k') => Set k -> (k -> m k') -> m (Set k')
set_map_m set f = do
  xs <- mapM f # Set.toList set
  return # Set.fromList xs

logb' :: String
logb' = put_in2 "\n" # replicate 100 '='

logb :: IO ()
logb = putStrLn logb'

starts_with :: (Eq a) => [a] -> [a] -> Prop
starts_with xs ys = case xs of
  [] -> True
  (x : xs) -> case ys of
    [] -> False
    (y : ys) -> x == y && starts_with xs ys

ends_with :: (Eq a) => [a] -> [a] -> Prop
ends_with xs ys = starts_with (reverse xs) (reverse ys)

includes :: (Eq a) => [a] -> [a] -> Prop
includes xs ys = any (starts_with xs) # tails ys

extract_middle :: (Show a, Eq a) => [a] -> [a] -> [a] -> Maybe [a]
extract_middle xs_start xs_end xs = do
  let n1 = length xs_start
  let n2 = length xs_end
  let n = length xs - n1 - n2
  True <- pure # n >= 0
  (xs_start', xs) <- pure # splitAt n1 xs
  True <- pure # xs_start' == xs_start
  (xs, xs_end') <- pure # splitAt n xs
  True <- pure # xs_end' == xs_end
  return xs

rev_map :: (Ord k, Ord v) => Map k v -> Map v k
rev_map = Map.fromList . map swap . Map.toList

asrt :: HCS => (MonadFail m) => Prop -> m ()
asrt p = ifmn p # fail show_stack

list_head_tail :: (Inhabited a) => [a] -> (a, [a])
list_head_tail xs = case xs of
  [] -> (dflt, [])
  (x : xs) -> (x, xs)

until_m :: (Monad m) => m Prop -> m ()
until_m f = f >>= \p -> ifmn p # until_m f

class MonadExt m1 m2 where
  monad_ext :: m1 a -> m2 a

instance MonadExt m m where
  monad_ext = id

-- instance MonadExt IO IO where
--   monad_ext = id

state_map_get :: (Monad m, Ord k) => m v -> k -> Map k v -> m (v, Map k v)
state_map_get m k mp = case map_get k mp of
  Just v -> pure (v, mp)
  Nothing -> do
    v <- m
    return (v, map_insert k v mp)

state_map_getm :: (Monad m, Ord k) => m v -> k -> StateT (Map k v) m v
state_map_getm m k = StateT # state_map_get m k

-- foldrM :: (Monad m) => (a -> b -> m b) -> b -> [a] -> m b
-- foldrM f z xs = case xs of
--   [] -> pure z
--   (x : xs) -> do
--     y <- foldrM f z xs
--     f x y

foldrM1 :: (MonadFail m) => (a -> a -> m a) -> [a] -> m a
foldrM1 f xs = case xs of
  [] -> fail "foldrM1"
  [x] -> pure x
  (x : xs) -> do
    y <- foldrM1 f xs
    f x y

print :: (MonadIO m, Show a) => a -> m ()
print a = liftIO # Prelude.print a

putStrLn :: (MonadIO m) => String -> m ()
putStrLn s = liftIO # Prelude.putStrLn s

getLine :: (MonadIO m) => m String
getLine = liftIO Prelude.getLine

newtype Wrap a = Wrap a

instance Eq (Wrap a) where
  a == b = True

instance Ord (Wrap a) where
  compare a b = EQ

instance Show (Wrap a) where
  show a = "(...)"

instance Num Prop where
  negate p = undefined
  (+) = (/=)
  (-) = (/=)
  (*) = (&&)
  abs p = p
  signum p = p
  fromInteger n = case n of
    1 -> True
    0 -> False
    _ -> error ""

list_to_ico :: (Num n, Enum n) => [a] -> [n]
list_to_ico = zipWith const [0..]

pairs :: [a] -> [(a, a)]
pairs xs = case xs of
  [] -> []
  (x : xs) -> map (mk_pair x) xs ++ pairs xs

pairs_with :: (a -> a -> b) -> [a] -> [b]
pairs_with f xs = case xs of
  [] -> []
  (x : xs) -> map (f x) xs ++ pairs_with f xs

while :: (Monad m) => m Prop -> m () -> m ()
while f g = f >>= \h -> ifm h # g >> while f g

noimpl :: HCS => a
noimpl = error "Not implemented"

filter_justs :: [Maybe a] -> [a]
filter_justs xs = do
  Just x <- xs
  return x

map_alter :: (Ord k) => k -> Map k v -> (Maybe v -> Maybe v) -> Map k v
map_alter k mp f = Map.alter f k mp

map_alter' :: (Ord k) => k -> Map k v -> (Maybe v -> v) -> Map k v
map_alter' k mp f = Map.alter (pure . f) k mp

type MapSet a b = Map a (Set b)
type MapMap a b c = Map a (Map b c)

map_set_insert :: (Ord a, Ord b) => a -> b -> MapSet a b -> MapSet a b
map_set_insert a b mp = map_alter' a mp #
  maybe (Set.singleton b) (set_insert b)

map_set_remove :: (Ord a, Ord b) => a -> b -> MapSet a b -> MapSet a b
map_set_remove a b mp = map_alter a mp # \set -> do
  set <- fmap (Set.delete b) set
  asrt # not # Set.null set
  return set

map_map_insert :: (Ord a, Ord b) => a -> b -> c -> MapMap a b c -> MapMap a b c
map_map_insert a b c mp = map_alter' a mp #
  maybe (Map.singleton b c) (map_insert b c)

map_set_insert_new :: HCS => (Ord a, Ord b) => a -> b -> MapSet a b -> MapSet a b
map_set_insert_new a b mp = case Map.lookup a mp of
  Nothing -> map_insert a (Set.singleton b) mp
  Just set -> let
    !set' = set_insert_new b set
    in map_insert a set' mp

map_map_insert_new :: HCS => (Ord a, Ord b) => a -> b -> c -> MapMap a b c -> MapMap a b c
map_map_insert_new a b c mp = case Map.lookup a mp of
  Nothing -> map_insert a (Map.singleton b c) mp
  Just mp1 -> let
    !mp1' = map_insert_new b c mp1
    in map_insert a mp1' mp

map_map_remove :: (Ord a, Ord b) => a -> b -> MapMap a b c -> MapMap a b c
map_map_remove a b mp = map_alter a mp # \mp -> do
  mp <- fmap (Map.delete b) mp
  asrt # not # Map.null mp
  return mp

from_just_m :: (MonadFail m) => m (Maybe a) -> m a
from_just_m m = do
  Just a <- m
  return a

maybe_to_dflt :: (Inhabited a) => Maybe a -> a
maybe_to_dflt = fromMaybe dflt

map_get_dflt :: (Ord k, Inhabited v) => k -> Map k v -> v
map_get_dflt k mp = maybe_to_dflt # map_get k mp

nub_sort :: (Ord a) => [a] -> [a]
nub_sort = Set.toList . Set.fromList

nub_sort_by :: (Eq a) => (a -> a -> Ordering) -> [a] -> [a]
nub_sort_by f xs = sortBy f # nub xs

nub_sort_by_ord :: (Eq a, Ord b) => (a -> b) -> [a] -> [a]
nub_sort_by_ord f xs = sortBy <~ nub xs # \a b -> compare (f a) (f b)

list_map_adj_cycle :: [a] -> (a -> a -> a -> b) -> [b]
list_map_adj_cycle xs f = if null xs then [] else let
  n = length xs
  ys = cycle xs
  in zipWith3 f (drop (n - 1) ys) xs (drop 1 ys)

set_bind :: (Ord a, Ord b) => Set a -> (a -> Set b) -> Set b
set_bind set f = Set.unions # map f # Set.toList set

set_bind' :: (Ord a, Ord b) => (a -> Set b) -> Set a -> Set b
set_bind' = flip set_bind

bimap2 :: (Bifunctor f) => (a -> b) -> f a a -> f b b
bimap2 f = bimap f f

converge :: (Eq a) => (a -> a) -> a -> [a]
converge f x = x : do
  let y = f x
  asrt # y /= x
  converge f y
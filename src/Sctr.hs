module Sctr where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)

import Util

type Sctr1 = (String, String)
type Sctr = [Sctr1]

class (MonadError Sctr m) => MonadShow a m where
  m_show :: a -> m String

scope_dflt = "dflt"
scope_err = "err"

show_sctr :: Sctr -> String
show_sctr msg = msg >>= snd

dfsc :: String -> Sctr1
dfsc = mk_pair scope_dflt

err :: (MonadError Sctr m) => Sctr -> m a
err msg = raise # (scope_err, "[Error] ") : msg

sctr_add_char :: Name -> Char -> Sctr -> Sctr
sctr_add_char scope c sctr = case sctr of
  [] -> [(scope, [c])]
  ((scope0, str0) : sctr') -> if scope == scope0
    then (scope0, c : str0) : sctr'
    else (scope, [c]) : sctr

sctr_add_str :: Name -> String -> Sctr -> Sctr
sctr_add_str scope s0 sctr = let
  s = reverse s0
  in case sctr of
    [] -> [(scope, s)]
    ((scope0, str0) : sctr') -> if scope == scope0
      then (scope0, s ++ str0) : sctr'
      else (scope, s) : sctr

sctr_drop :: N -> Sctr -> Sctr
sctr_drop n ss = if n == 0 then ss else case ss of
  [] -> []
  ((sc, s) : ss) -> let
    k = len s
    in if n < k then (sc, drop (fi n) s) : ss
      else sctr_drop (n - k) ss
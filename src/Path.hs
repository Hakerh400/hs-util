module Path where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative
import Control.Monad
import System.Directory

import Util

pth_to_list :: FilePath -> [String]
pth_to_list pth = split_at_elem '/' pth >>= split_at_elem '\\'

list_to_pth :: [String] -> FilePath
list_to_pth = intercalate "/"

pth_lift :: ([String] -> [String]) -> FilePath -> FilePath
pth_lift f pth = list_to_pth # f # pth_to_list pth

pthl_rm_nulls' :: [String] -> [String]
pthl_rm_nulls' = filter # not . null

pthl_rm_nulls :: [String] -> [String]
pthl_rm_nulls xs = case xs of
  [] -> []
  (x:xs) -> x : pthl_rm_nulls' xs

pthl_cancel_dirs' :: [String] -> [String]
pthl_cancel_dirs' pth = case pth of
  [] -> []
  (x : "." : xs) -> pthl_cancel_dirs' # x : xs
  (x : ".." : xs) -> let next = pthl_cancel_dirs' xs in
    if x == ".." then ".." : ".." : next else next
  (x : xs) -> x : pthl_cancel_dirs' xs

pthl_cancel_dirs :: [String] -> [String]
pthl_cancel_dirs pth = let pth1 = pthl_cancel_dirs' pth
  in if pth1 == pth then pth else pthl_cancel_dirs pth1

pthl_norm :: [String] -> [String]
pthl_norm pth = pthl_cancel_dirs # pthl_rm_nulls pth

pth_norm :: FilePath -> FilePath
pth_norm = pth_lift pthl_norm

pth_add_ext :: String -> String -> String
pth_add_ext s e = concat [s, ".", e]

pth_join :: FilePath -> FilePath -> FilePath
pth_join pth1 pth2 = pth_norm # concat [pth1, "/", pth2]

pth_joins :: FilePath -> [FilePath] -> FilePath
pth_joins = foldl' pth_join

pth_concat :: [FilePath] -> FilePath
pth_concat = foldr1 pth_join

pth_rel_list :: [String] -> [String] -> [String]
pth_rel_list xs ys = let (_, a, b) = common_prefix xs ys in
  map (const "..") a ++ b

pth_rel :: FilePath -> FilePath -> FilePath
pth_rel pth1 pth2 = list_to_pth #
  pth_rel_list (pth_to_list pth1) (pth_to_list pth2)

pth_parent :: FilePath -> FilePath
pth_parent pth = pth_join pth ".."

pth_base :: FilePath -> Name
pth_base pth = let pth' = pth_norm pth in
  if null pth' then pth' else
    last # split_at_elem '/' pth'

pth_name_ext :: FilePath -> (Name, Maybe Name)
pth_name_ext pth = let base = pth_base pth in
  if null base then ("", Nothing) else
    let (name : xs) = split_at_elem '.' base
    in (name, lastm xs)

pth_name :: FilePath -> Name
pth_name = fst . pth_name_ext

pth_ext :: FilePath -> Maybe Name
pth_ext = snd . pth_name_ext
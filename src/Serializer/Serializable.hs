{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Serializer.Serializable where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Data.Time.Clock
import Data.Time.Calendar
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Control.Monad.Except hiding (fix)

import Util
import Sctr
import Bit
import Serializer.Util
import Serializer.Ser
import Serializer.Dser

class Serializable a where
  ser :: a -> Ser ()
  dser :: Dser a

ser_to_buf' :: Ser () -> Either Sctr Buf
ser_to_buf' f = evalStateT (f >> ser_get_output) ser_init

dser_from_buf' :: Dser a -> Buf -> Either Sctr a
dser_from_buf' f buf = evalStateT f # dser_init buf

ser_to_buf :: (Serializable a) => a -> Buf
ser_to_buf a = case ser_to_buf' # ser a of
  Left msg -> error # show msg
  Right buf -> buf

dser_from_buf :: (Serializable a) => Buf -> a
dser_from_buf buf = case dser_from_buf' dser buf of
  Left msg -> error # show msg
  Right a -> a

reser_aux :: (a -> Ser ()) -> Dser a -> a -> Either Sctr a
reser_aux f g a = do
  buf <- evalStateT (f a >> ser_get_output) ser_init
  evalStateT g # dser_init buf

reser' :: (Serializable a) => a -> Either Sctr a
reser' = reser_aux ser dser

reser :: (MonadError Sctr m, Eq a, Show a, Serializable a) => a -> m a
reser a = case reser' a of
  Left msg -> err msg
  Right b -> if a == b then pure a else
    err [dfsc # show (a, b)]

ser_0 :: Ser ()
ser_0 = ser_bit 0

ser_1 :: Ser ()
ser_1 = ser_bit 1

instance Serializable Bit where
  ser = ser_bit
  dser = dser_bit

ser_prop :: Prop -> Ser ()
ser_prop p = ser # prop_to_bit p

dser_prop :: Dser Prop
dser_prop = dser_bit >>= liftm bit_to_prop

instance Serializable Prop where
  ser = ser_prop
  dser = dser_prop

ser_list_lin :: (a -> Ser ()) -> [a] -> Ser ()
ser_list_lin f xs = case xs of
  [] -> ser_0
  x : xs -> ser_1 >> f x >> ser_list_lin f xs

dser_list_lin :: Dser a -> Dser [a]
dser_list_lin f = dser_bit >>= \bit -> case bit of
  0 -> pure []
  1 -> do
    x <- f
    xs <- dser_list_lin f
    return # x : xs
  _ -> error ""

ser_list :: (a -> Ser ()) -> [a] -> Ser ()
ser_list f xs = do
  ser_nat' # len xs
  mapM_ f xs

dser_list :: Dser a -> Dser [a]
dser_list f = do
  n <- dser_nat'
  replicateM (fi n) f

instance (Serializable a) => Serializable [a] where
  ser = ser_list ser
  dser = dser_list dser

nat_to_bits_fn :: N -> (Bit, N)
nat_to_bits_fn n = (fst_bit n, shiftR n 1)

nat_to_bits :: N -> [Bit]
nat_to_bits n = iter_st_until (== 1) nat_to_bits_fn # n + 1

bits_to_nat :: [Bit] -> N
bits_to_nat bits = bits_to_integral' bits - 1

ser_nat' :: N -> Ser ()
ser_nat' n = ser_list_lin ser # nat_to_bits n

dser_nat' :: Dser N
dser_nat' = dser_list_lin dser >>= liftm bits_to_nat

ser_nat :: N -> Ser ()
ser_nat = ser

dser_nat :: Dser N
dser_nat = dser

ser_int :: N -> Ser ()
ser_int n = if n >= 0
  then ser_0 >> ser_nat' n
  else ser_1 >> ser_nat' (-n - 1)

dser_int :: Dser N
dser_int = do
  b <- dser_bit
  n <- dser_nat'
  return # case b of
    0 -> n
    1 -> -n - 1
    _ -> error ""

instance Serializable N where
  ser = ser_int
  dser = dser_int

instance Serializable Int where
  ser = ser_int . fi
  dser = fmap fi dser_int

ser_char :: Char -> Ser ()
ser_char c = ser_nat' # fi # ord c

dser_char :: Dser Char
dser_char = dser_nat' >>= liftm (chr . fi)

instance Serializable Char where
  ser = ser_char
  dser = dser_char

ser_pair :: (a -> Ser ()) -> (b -> Ser ()) -> (a, b) -> Ser ()
ser_pair f g (a, b) = f a >> g b

dser_pair :: Dser a -> Dser b -> Dser (a, b)
dser_pair f g = do
  a <- f
  b <- g
  return (a, b)

instance (Serializable a, Serializable b) => Serializable (a, b) where
  ser = ser_pair ser ser
  dser = dser_pair dser dser

instance (Serializable a, Serializable b, Serializable c) =>
  Serializable (a, b, c) where
  ser (a, b, c) = ser a >> ser b >> ser c
  dser = do
    a <- dser
    b <- dser
    c <- dser
    return (a, b, c)

ser_set :: (Ord a) => (a -> Ser ()) -> Set a -> Ser ()
ser_set f s = ser_list f # Set.toList s

dser_set :: (Ord a) => Dser a -> Dser (Set a)
dser_set f = dser_list f >>= liftm Set.fromList

instance (Ord a, Serializable a) => Serializable (Set a) where
  ser = ser_set ser
  dser = dser_set dser

ser_map :: (Ord a) => (a -> Ser ()) -> (b -> Ser ()) -> Map a b -> Ser ()
ser_map f g mp = ser_list (ser_pair f g) # Map.toList mp

dser_map :: (Ord a) => Dser a -> Dser b -> Dser (Map a b)
dser_map f g = dser_list (dser_pair f g) >>= liftm Map.fromList

instance (Ord a, Serializable a, Serializable b) => Serializable (Map a b) where
  ser = ser_map ser ser
  dser = dser_map dser dser

ser_bint :: N -> N -> N -> Ser ()
ser_bint n_min n_max n = if n_min == n_max then nop else do
  let n_mid = shiftR (n_min + n_max) 1
  if n <= n_mid
    then ser_0 >> ser_bint n_min n_mid n
    else ser_1 >> ser_bint (n_mid + 1) n_max n

dser_bint :: N -> N -> Dser N
dser_bint n_min n_max = if n_min == n_max then pure n_max else do
  let n_mid = shiftR (n_min + n_max) 1
  gt <- dser_prop
  if not gt
    then dser_bint n_min n_mid
    else dser_bint (n_mid + 1) n_max

ser_bnat :: N -> N -> Ser ()
ser_bnat = ser_bint 0

dser_bnat :: N -> Dser N
dser_bnat = dser_bint 0

ser_bnat' :: N -> N -> Ser ()
ser_bnat' n_max1 = ser_bnat # n_max1 - 1

dser_bnat' :: N -> Dser N
dser_bnat' n_max1 = dser_bnat # n_max1 - 1

ser_maybe :: (a -> Ser ()) -> Maybe a -> Ser ()
ser_maybe f m = case m of
  Nothing -> ser_0
  Just a -> ser_1 >> f a

dser_maybe :: Dser a -> Dser (Maybe a)
dser_maybe f = dser_bit >>= \b -> case b of
  0 -> pure Nothing
  1 -> f >>= liftm Just
  _ -> error ""

instance (Serializable a) => Serializable (Maybe a) where
  ser = ser_maybe ser
  dser = dser_maybe dser

ser_either :: (a -> Ser ()) -> (b -> Ser ()) -> Either a b -> Ser ()
ser_either f g x = case x of
  Left a -> ser_0 >> f a
  Right a -> ser_1 >> g a

dser_either :: Dser a -> Dser b -> Dser (Either a b)
dser_either f g = dser_bit >>= \b -> case b of
  0 -> f >>= liftm Left
  1 -> g >>= liftm Right
  _ -> error ""

instance (Serializable a, Serializable b) => Serializable (Either a b) where
  ser = ser_either ser ser
  dser = dser_either dser dser

ser_enum :: (Enum a) => a -> Ser ()
ser_enum x = ser_nat' # fi # fromEnum x

dser_enum :: (Enum a) => Dser a
dser_enum = do
  n <- dser_nat'
  return # toEnum # fi n

-- ser_diff_time :: DiffTime -> Ser ()
-- ser_diff_time t = ser # diffTimeToPicoseconds t
-- 
-- dser_diff_time :: Dser DiffTime
-- dser_diff_time = dser >>= liftm picosecondsToDiffTime
-- 
-- instance Serializable DiffTime where
--   ser = ser_diff_time
--   dser = dser_diff_time
-- 
-- ser_day :: Day -> Ser ()
-- ser_day (ModifiedJulianDay n) = ser n
-- 
-- dser_day :: Dser Day
-- dser_day = dser >>= liftm ModifiedJulianDay
-- 
-- instance Serializable Day where
--   ser = ser_day
--   dser = dser_day
-- 
-- ser_utc_time :: UTCTime -> Ser ()
-- ser_utc_time t = do
--   ser # utctDay t
--   ser # utctDayTime t
-- 
-- dser_utc_time :: Dser UTCTime
-- dser_utc_time = do
--   day <- dser
--   day_time <- dser
--   return # UTCTime
--     { utctDay = day
--     , utctDayTime = day_time
--     }
-- 
-- instance Serializable UTCTime where
--   ser = ser_utc_time
--   dser = dser_utc_time

ser_ordering :: Ordering -> Ser ()
ser_ordering cmp = ser_bnat 2 # case cmp of
  LT -> 0
  EQ -> 1
  GT -> 2

dser_ordering :: Dser Ordering
dser_ordering = dser_bnat 2 >>= \k -> pure # case k of
  0 -> LT
  1 -> EQ
  2 -> GT
  _ -> error ""

instance Serializable Ordering where
  ser = ser_ordering
  dser = dser_ordering
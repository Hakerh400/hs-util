module Serializer.Ser where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Monad.State

import Util
import Sctr
import Bit
import Serializer.Util

data SerializerT = SerializerT
  { _ser_bits :: [Bit]
  } deriving (Eq, Ord, Show)

type Ser = StateT SerializerT (Either Sctr)

ser_init :: SerializerT
ser_init = SerializerT
  { _ser_bits = []
  }

ser_get_output :: Ser Buf
ser_get_output = do
  bits <- gets _ser_bits
  return # bits_to_buf{-_trim-} # reverse bits

ser_bit :: Bit -> Ser ()
ser_bit b = modify # \ser ->
  ser {_ser_bits = b : _ser_bits ser}
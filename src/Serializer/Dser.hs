module Serializer.Dser where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Monad.State

import Util
import Sctr
import Bit
import Serializer.Util

data DeserializerT = DeserializerT
  { _dser_bits :: [Bit]
  } deriving (Eq, Ord, Show)

type Dser = StateT DeserializerT (Either Sctr)

dser_init :: Buf -> DeserializerT
dser_init buf = DeserializerT
  { _dser_bits = buf_to_bits buf
  }

dser_bit :: Dser Bit
dser_bit = do
  bits <- gets _dser_bits
  modify # \dser -> dser {_dser_bits = tail' bits}
  return # head' 0 bits
module Serializer.Util where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative
import Control.Monad

import Util
import Bit

fst_bit :: (Integral a) => a -> Bit
fst_bit n = prop_to_bit # odd n

bits_to_integral_aux :: (Integral a, Bits a) => a -> [Bit] -> a
bits_to_integral_aux = foldr (\b bt -> shiftL bt 1 .|. fi b)

bits_to_integral :: (Integral a, Bits a) => [Bit] -> a
bits_to_integral = bits_to_integral_aux 0

bits_to_integral' :: (Integral a, Bits a) => [Bit] -> a
bits_to_integral' = bits_to_integral_aux 1

bits_to_byte :: [Bit] -> Byte
bits_to_byte = bits_to_integral

byte_to_bits_fn :: Byte -> (Bit, Byte)
byte_to_bits_fn bt = (fst_bit bt, shiftR bt 1)

byte_to_bits :: Byte -> [Bit]
byte_to_bits bt = take 8 # iter_st byte_to_bits_fn bt

bits_to_bytes :: [Bit] -> [Byte]
bits_to_bytes bits = map bits_to_byte #
  split_into_same_groups_pad_end 8 0 bits

bits_to_buf :: [Bit] -> Buf
bits_to_buf bits = BS.pack # bits_to_bytes bits

bits_to_buf_trim :: [Bit] -> Buf
bits_to_buf_trim bits = BS.pack # trim_end 0 # bits_to_bytes bits

buf_to_bits :: Buf -> [Bit]
buf_to_bits buf = BS.unpack buf >>= byte_to_bits

hex_digit_to_char :: N -> Char
hex_digit_to_char d = let h = d <= 9 in
  fromJust # list_get (ite h d (d - 10)) [ite h '0' 'A' ..]

show_byte :: Byte -> String
show_byte bt = map (hex_digit_to_char . fi) [shiftR bt 4, bt .&. 3]

show_buf :: Buf -> String
show_buf buf = BS.unpack buf >>= show_byte